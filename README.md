# DFIM Vector control with MATLAB Embedded Coder

An undergraduate senior project, porting doubly-fed induction machine vector control system to TMS320F28335 with MATLAB/Simulink Embedded Coder.
It wasn't finished due to COVID-19.

Author contact: pp@myo.in.th  
Advisor: Asst. Prof. Surapong Suwankawin

Online link: https://gitlab.com/inganault/senior-project-dfim-embedded-coder

## Table of Contents <!-- omit in toc -->

- [Hardware](#hardware)
  - [Overview](#overview)
  - [Wiring](#wiring)
  - [Current sensor](#current-sensor)
  - [Interface board](#interface-board)
    - [Connections](#connections)
- [Software](#software)
  - [Simulink / Embedded Coder](#simulink--embedded-coder)
  - [UART Monitor](#uart-monitor)
- [Reports and Slides](#reports-and-slides)
- [Possible Improvements & Known Problems](#possible-improvements--known-problems)
- [References / Past works on this system](#references--past-works-on-this-system)
- [Appendix](#appendix)
  - [Files format / Used software](#files-format--used-software)
  - [Development Environment Installation](#development-environment-installation)
  - [F28335 Resource links](#f28335-resource-links)
  - [Photos](#photos)

## Hardware

### Overview

![Boards](pictures/boards_hilight.svg)

The system is based on previous works in PERL lab. New components are highlighted. Only new boards' schematics are included in this repository.

- **Inverter** - Modified FRECON iX (10 HP Model). IGBT Module is PM50CSE120.
- **Rectifier** - Modified FRECON iH (10 HP Model).
- **DFIM** - 4kW wound rotor motor
- **Current sensor** (new) - LAH-25NP with in-house PCB.
- **Voltage sensor** - Step-down transformer with op-amp buffering.
- **PWM Buffer** - Simple 74LS07 level shifter.
- **Encoder Buffer** - Opto-isolator.
- **TMS320F28335** (new) - Microcontroller.
- **DAC** - 8 channel DAC based on TLV5630.

### Wiring

![HV Wiring](pictures/wiring_power.jpg)
_Wiring for high voltage cable_

![LV Wiring](pictures/wiring.svg)
_Wiring for measurement & control signals_ ([compare with photo >here<](#control_pane))

### Current sensor

<img src="pictures/current_sensor_front.png" width="45%">
<img src="pictures/current_sensor_real.jpg" width="45%">

- Based on LAH25-NP current transducer
- Supply: +15 V, -15 V
- Output: 2x5 pinheader, BNC jack
- Adjust offset with R9 (Offset). The offset is designed to center around +1.5V.
- Adjust gain by
  - Changing CT configuration (resolder jumpers)
  - Swapping R6
- $`Gain = {Gain}_{CT} \cdot R_1 \cdot \frac{R_7}{R_6} \quad [V/A]`$
  - $`{Gain}_{CT} = 1:1000, R_1 = 235 \;\Omega, R_6 = 6.2k\Omega, R_7 = 1k\Omega`$
  - $`Gain = 37.9 \;[mV/A] = 51.75 \;[LSB/A]`$
- The board can also handle Sallen-Key filter. See schematics.

### Interface board

<img src="pictures/interface.png" width="45%">
<img src="pictures/interface_real.jpg" width="45%">

For connecting F28335 card with other boards.

F28335 card use DIMM100 connector, an obsolete connector which is hard to find.
The card has built-in ADC protection diodes. So, only RC low pass is added to give more protection and out-of-band noise attenuation.

#### Connections

- **DC jack** - 5V ~ 12V, add or bypass regulator according to the input voltage.
- **JTAG** - for programming with XDS100 programmer.
- **UART** - for debugging purposes. connect it to an UART-to-USB adapter.
- **ADC** - from ADC splitter board.
- **QEP1** - from encoder buffer board.
- **DAC** - to DAC board.
- **PWM** - to PWM buffer.
- **QEP2 & PWM2** - spare ports can be used as GPIO.

## Software

### Simulink / Embedded Coder

Simulation and embedded coder files are separated to ease debugging.
They can be combined back when major bugs are fixed.

Please load bus.mat file before running simulation or embedded coder.
The .slx files have been edited in both MATLAB R2019a and R2019b.
If there is any compilation error or incompatibilities please contact the author.

- **DFIM Simulation** - DFIM model taken from [[1]](#footnote1). \$`K_P`$ and $`K_I`\$ can be adjusted for better response.
- **V/F Test** - For testing hardware. Connect inverter output to stator and short rotor coils.
- **DFIM Test** - The main objective of the project but it is not working. It might be because of wrong deadtime compensation.

Peripheral configuration should be correct with some notes below.

- **sci_tx & sci_rx** - custom S-Function block to due to TI's block being unusable.
- **SPI Transmit** - TI's block can not toggle chip select signal multiple times in single execution, resulting in only one channel per execution can be sent to DAC. Writing custom S-Function will solve this problem.
- **Encoder / QEP** - No automatic pole alignment, offset need to be set manually. Require at least 1 revolution after program startup to track position correctly.

### UART Monitor

<img src="pictures/uart_mon.png" width="45%">

Used for displaying real-time data and sending commands.
Not user friendly, most configuration is hard-coded.
LightningChart, a required library of this program is not included in this repository due to licensing.
To obtain it, please contact the author.

## Reports and Slides

The proposal have uncorrected errors and typos. The final report has some correction below, otherwise should be correct.

### Equation 1~5

```math
\begin{align}

\frac{d\vec{i_s}}{dt} &= - \frac{R_s}{L_s} \vec{i_s} - \frac{M}{L_s}\frac{d}{dt}\left( e^{J\theta_e} \vec{i'_r} \right) + \frac{\vec{v_s}}{L_s} \\

\vec{\lambda_s} &= L_s \vec{i_s} + M \left( e^{J\theta_e} \vec{i'_r} \right) \\

\frac{d\vec{i'_r}}{dt} &= - \frac{R_r}{L_r} \vec{i'_r} - \frac{M}{L_r}\frac{d}{dt}\left( e^{-J\theta_e} \vec{i_s} \right) + \frac{\vec{v'_r}}{L_r} \\

\vec{\lambda'_r} &= L_r \vec{i'_r} + M \left( e^{-J\theta_e} \vec{i_s} \right) \\

\tau_m &= -p \left(\vec{i_s} \times \vec{\lambda_s}\right) \\

\end{align}
```

### Definition

- Subscript $`'`$ : Value in rotor reference frame

## Possible Improvements & Known Problems

- **Deadtime compensation is wrong.**
- Add DC bus voltage sensor to calculate PWM duty cycle accurately.
- C2000 Support Package's SPI Block cannot transmit all ADC channels simultaneously.
- The current program calculate velocity from unwrapped angular position. The unwrapped angular position will lose precision over time. It should be calculated directly from the angular position.
- Control loop frequency is not constant due to unnecessary calculation from both high-frequency and low-frequency loop. The entire program needs some optimization to run smoothly.
- The voltage sensor PCB doesn't have input protection. The op-amp can be destroyed when a transient voltage event occurs. Adding a series resistor in front of op-amp helps.

## References / Past works on this system

1. <a name="footnote1"></a>ศุภษร หมื่นพล, “ระบบควบคุมเวกเตอร์แบบไร้เซนเซอร์วัดตำแหน่งสำหรับเครื่องจักรกลไฟฟ้าเหนี่ยวนำชนิดป้อนสองทางโดยใช้ตัวสังเกตลดอันดับแบบปรับตัวที่มีการป้อนกลับกระแสสเตเตอร์”.
   วิทยานิพนธ์ วศ.ม., จุฬาลงกรณ์มหาวิทยาลัย, 2561.  
   https://cuir.car.chula.ac.th/bitstream/123456789/63581/1/5870252421.pdf

2. สมรัฐ สมิทธิสมบูรณ์, “การควบคุมกำลังรีแอกทีฟทางด้านโรเตอร์สำหรับระบบควบคุมเวกเตอร์ไร้เซนเซอร์วัดตำแหน่งของเครื่องจักรกลไฟฟ้าเหนี่ยวนำชนิดป้อนสองทาง”.
   วิทยานิพนธ์ วศ.ม., จุฬาลงกรณ์มหาวิทยาลัย, 2556.  
   https://cuir.car.chula.ac.th/bitstream/123456789/43671/1/5370411421.pdf

3. จิรัฏฐ์ อุดมศรี, “ระบบควบคุมเวกเตอร์แบบไร้เซนเซอร์วัดตำแหน่งสำหรับเครื่องจักรกลไฟฟ้าเหนี่ยวนำ ชนิดป้อนสองทางด้วยตัวสังเกตลดอันดับแบบปรับตัว”.
   การประชุมวิชาการทางวิศวกรรมไฟฟ้า ครั้งที่ 34; 2554. หน้า. 493-6.  
   https://cuir.car.chula.ac.th/bitstream/123456789/43671/1/5370411421.pdf

## Appendix

### Files format / Used software

- PCB design (.sch, .brd, .lbr): Autodesk EAGLE 9
- PCB Gerber: gerbv
- Simulink (.slx): MATLAB 2019a / 2019b
- C# (.cs): Visual Studio 2019

### Development Environment Installation

Tested version denoted in parenthesis.

1. Install [C2000Ware](https://www.ti.com/tool/C2000WARE) (2.01.00.00)
2. Install [Code Composer Studio](https://www.ti.com/tool/CCSTUDIO) (9.3.0.00012)
3. Install [MATLAB](https://www.mathworks.com/) (R2019b) with at least
   - Simulink
   - MATLAB Coder
   - Simulink Coder
   - Embedded Coder
   - Simscape
   - Simscape Electrical
   - Signal Processing Toolbox
   - DSP System Toolbox
   - Control System Toolbox
4. Install add-on `Embedded Coder Support Package for Texas Instrument C2000 Processors` (19.2.2) in MATLAB
5. When creating a new Simulink file, uncheck `Enable FastRTS` in hardware configuration.

### F28335 Resource links

- [TMS320F2833x, TMS320F2823x Digital Signal Controllers (DSCs)](http://www.ti.com/lit/pdf/sprs439)
- [TMS320x2833x, 2823x system control and interrupts reference guide](http://www.ti.com/lit/pdf/SPRUFB0)
- [TMS320x2833x Analog-to-Digital Converter (ADC) module reference guide](http://www.ti.com/lit/pdf/SPRU812)
- [TMS320x2833x, 2823x Enhanced Pulse Width Modulator (ePWM) module reference guide](http://www.ti.com/lit/pdf/SPRUG04)
- [TMS320x2833x, 2823x Enhanced Quadrature Encoder Pulse (eQEP) module reference guide](http://www.ti.com/lit/pdf/SPRUG05)
- [TMS320x2833x, 2823x Serial Communications Interface (SCI) reference guide](http://www.ti.com/lit/pdf/SPRUFZ5)
- [TMS320x2833x, 2823x Serial Peripheral Interface (SPI) reference guide](http://www.ti.com/lit/pdf/SPRUEU3)
- [Programming TMS320x28xx and TMS320x28xxx Peripherals in C/C++](http://www.ti.com/lit/pdf/spraa85e)
- [F28335-HWDevPkg-controlCARD schematics,BOM,pin-out table,Gerber files 2.0 (Rev. C)](http://www.ti.com/lit/zip/sprr102)

### Photos

![Motor & Load](pictures/equipment_motor.jpg)
_From left to right: Variac, DFIM, Torque meter, and Load_

![Equipment Rack](pictures/equipment_rack.jpg)
_Equipment Rack_

![Inverter](pictures/equipment_inverter.jpg)
_Inverter_

![Converter](pictures/equipment_converter.jpg)
_Converter/Rectifier_

![Controls](pictures/control_pane.jpg)
<a name="control_pane"></a>_Low voltage section_

﻿namespace uart_monitor
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.refreshBtn = new System.Windows.Forms.Button();
            this.connectBtn = new System.Windows.Forms.Button();
            this.serialPortList = new System.Windows.Forms.ComboBox();
            this.textRate = new System.Windows.Forms.TextBox();
            this.serialPort = new System.IO.Ports.SerialPort(this.components);
            this.chart1 = new Arction.WinForms.Charting.LightningChartUltimate();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textCH1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textCH4 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textCH3 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textCH2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textCH6 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textCH7 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textCH8 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textCH5 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonResetZoom = new System.Windows.Forms.Button();
            this.textCursorPos2 = new System.Windows.Forms.TextBox();
            this.textCursorPos1 = new System.Windows.Forms.TextBox();
            this.buttonCursor2 = new System.Windows.Forms.Button();
            this.buttonCursor1 = new System.Windows.Forms.Button();
            this.labelCursor = new System.Windows.Forms.Label();
            this.textReject = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonSendCommand = new System.Windows.Forms.Button();
            this.textCmd1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textCmd4 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textCmd3 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.textCmd2 = new System.Windows.Forms.TextBox();
            this.comboDuration = new System.Windows.Forms.ComboBox();
            this.timerDraw = new System.Windows.Forms.Timer(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.comboTrig = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.radioTrig = new System.Windows.Forms.RadioButton();
            this.radioScroll = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // refreshBtn
            // 
            this.refreshBtn.Location = new System.Drawing.Point(182, 7);
            this.refreshBtn.Name = "refreshBtn";
            this.refreshBtn.Size = new System.Drawing.Size(70, 24);
            this.refreshBtn.TabIndex = 5;
            this.refreshBtn.Text = "Refresh";
            this.refreshBtn.UseVisualStyleBackColor = true;
            this.refreshBtn.Click += new System.EventHandler(this.refreshBtn_Click);
            // 
            // connectBtn
            // 
            this.connectBtn.Location = new System.Drawing.Point(110, 7);
            this.connectBtn.Margin = new System.Windows.Forms.Padding(2);
            this.connectBtn.Name = "connectBtn";
            this.connectBtn.Size = new System.Drawing.Size(68, 24);
            this.connectBtn.TabIndex = 4;
            this.connectBtn.Text = "Connect";
            this.connectBtn.UseVisualStyleBackColor = true;
            this.connectBtn.Click += new System.EventHandler(this.connectBtn_Click);
            // 
            // serialPortList
            // 
            this.serialPortList.FormattingEnabled = true;
            this.serialPortList.Location = new System.Drawing.Point(8, 9);
            this.serialPortList.Margin = new System.Windows.Forms.Padding(2);
            this.serialPortList.Name = "serialPortList";
            this.serialPortList.Size = new System.Drawing.Size(98, 21);
            this.serialPortList.TabIndex = 3;
            this.serialPortList.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.serialPortList_KeyPress);
            // 
            // textRate
            // 
            this.textRate.Location = new System.Drawing.Point(69, 18);
            this.textRate.Margin = new System.Windows.Forms.Padding(2);
            this.textRate.Name = "textRate";
            this.textRate.Size = new System.Drawing.Size(44, 20);
            this.textRate.TabIndex = 6;
            this.textRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // serialPort
            // 
            this.serialPort.BaudRate = 3125000;
            this.serialPort.ReadBufferSize = 8192;
            this.serialPort.ReadTimeout = 1000;
            this.serialPort.ReceivedBytesThreshold = 512;
            this.serialPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort_DataReceived);
            // 
            // chart1
            // 
            this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chart1.BackColor = System.Drawing.Color.White;
            this.chart1.Background = ((Arction.WinForms.Charting.Fill)(resources.GetObject("chart1.Background")));
            this.chart1.ChartManager = null;
            this.chart1.ColorTheme = Arction.WinForms.Charting.ColorTheme.LightGray;
            this.chart1.Location = new System.Drawing.Point(-2, 158);
            this.chart1.Margin = new System.Windows.Forms.Padding(4);
            this.chart1.MinimumSize = new System.Drawing.Size(82, 73);
            this.chart1.Name = "chart1";
            this.chart1.Options = ((Arction.WinForms.Charting.ChartOptions)(resources.GetObject("chart1.Options")));
            this.chart1.OutputStream = null;
            this.chart1.RenderOptions = ((Arction.WinForms.Charting.Views.RenderOptionsCommon)(resources.GetObject("chart1.RenderOptions")));
            this.chart1.Size = new System.Drawing.Size(841, 279);
            this.chart1.TabIndex = 7;
            this.chart1.Title = ((Arction.WinForms.Charting.Titles.ChartTitle)(resources.GetObject("chart1.Title")));
            this.chart1.View3D = ((Arction.WinForms.Charting.Views.View3D.View3D)(resources.GetObject("chart1.View3D")));
            this.chart1.ViewPie3D = ((Arction.WinForms.Charting.Views.ViewPie3D.ViewPie3D)(resources.GetObject("chart1.ViewPie3D")));
            this.chart1.ViewPolar = null;
            this.chart1.ViewSmith = null;
            this.chart1.ViewXY = ((Arction.WinForms.Charting.Views.ViewXY.ViewXY)(resources.GetObject("chart1.ViewXY")));
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Data rate";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(117, 20);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "samp/s";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.ForeColor = System.Drawing.Color.LimeGreen;
            this.label4.Location = new System.Drawing.Point(38, 43);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "CH1";
            // 
            // textCH1
            // 
            this.textCH1.Location = new System.Drawing.Point(69, 41);
            this.textCH1.Margin = new System.Windows.Forms.Padding(2);
            this.textCH1.Name = "textCH1";
            this.textCH1.Size = new System.Drawing.Size(60, 20);
            this.textCH1.TabIndex = 10;
            this.textCH1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label3.Location = new System.Drawing.Point(38, 111);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "CH4";
            // 
            // textCH4
            // 
            this.textCH4.Location = new System.Drawing.Point(69, 109);
            this.textCH4.Margin = new System.Windows.Forms.Padding(2);
            this.textCH4.Name = "textCH4";
            this.textCH4.Size = new System.Drawing.Size(60, 20);
            this.textCH4.TabIndex = 12;
            this.textCH4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(38, 89);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "CH3";
            // 
            // textCH3
            // 
            this.textCH3.Location = new System.Drawing.Point(69, 86);
            this.textCH3.Margin = new System.Windows.Forms.Padding(2);
            this.textCH3.Name = "textCH3";
            this.textCH3.Size = new System.Drawing.Size(60, 20);
            this.textCH3.TabIndex = 14;
            this.textCH3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.ForeColor = System.Drawing.Color.Turquoise;
            this.label6.Location = new System.Drawing.Point(38, 66);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "CH2";
            // 
            // textCH2
            // 
            this.textCH2.Location = new System.Drawing.Point(69, 63);
            this.textCH2.Margin = new System.Windows.Forms.Padding(2);
            this.textCH2.Name = "textCH2";
            this.textCH2.Size = new System.Drawing.Size(60, 20);
            this.textCH2.TabIndex = 16;
            this.textCH2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.ForeColor = System.Drawing.Color.DarkOrange;
            this.label7.Location = new System.Drawing.Point(164, 66);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "CH6";
            // 
            // textCH6
            // 
            this.textCH6.Location = new System.Drawing.Point(194, 63);
            this.textCH6.Margin = new System.Windows.Forms.Padding(2);
            this.textCH6.Name = "textCH6";
            this.textCH6.Size = new System.Drawing.Size(60, 20);
            this.textCH6.TabIndex = 24;
            this.textCH6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label8.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label8.Location = new System.Drawing.Point(164, 89);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "CH7";
            // 
            // textCH7
            // 
            this.textCH7.Location = new System.Drawing.Point(194, 86);
            this.textCH7.Margin = new System.Windows.Forms.Padding(2);
            this.textCH7.Name = "textCH7";
            this.textCH7.Size = new System.Drawing.Size(60, 20);
            this.textCH7.TabIndex = 22;
            this.textCH7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label9.ForeColor = System.Drawing.Color.DarkGreen;
            this.label9.Location = new System.Drawing.Point(164, 111);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "CH8";
            // 
            // textCH8
            // 
            this.textCH8.Location = new System.Drawing.Point(194, 109);
            this.textCH8.Margin = new System.Windows.Forms.Padding(2);
            this.textCH8.Name = "textCH8";
            this.textCH8.Size = new System.Drawing.Size(60, 20);
            this.textCH8.TabIndex = 20;
            this.textCH8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label10.ForeColor = System.Drawing.Color.DarkViolet;
            this.label10.Location = new System.Drawing.Point(164, 43);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "CH5";
            // 
            // textCH5
            // 
            this.textCH5.Location = new System.Drawing.Point(194, 41);
            this.textCH5.Margin = new System.Windows.Forms.Padding(2);
            this.textCH5.Name = "textCH5";
            this.textCH5.Size = new System.Drawing.Size(60, 20);
            this.textCH5.TabIndex = 18;
            this.textCH5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonResetZoom);
            this.groupBox1.Controls.Add(this.textCursorPos2);
            this.groupBox1.Controls.Add(this.textCursorPos1);
            this.groupBox1.Controls.Add(this.buttonCursor2);
            this.groupBox1.Controls.Add(this.buttonCursor1);
            this.groupBox1.Controls.Add(this.labelCursor);
            this.groupBox1.Controls.Add(this.textReject);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.textCH5);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.textRate);
            this.groupBox1.Controls.Add(this.textCH6);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textCH7);
            this.groupBox1.Controls.Add(this.textCH1);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textCH8);
            this.groupBox1.Controls.Add(this.textCH4);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textCH3);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textCH2);
            this.groupBox1.Location = new System.Drawing.Point(371, 9);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(459, 142);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Monitor";
            // 
            // buttonResetZoom
            // 
            this.buttonResetZoom.Location = new System.Drawing.Point(271, 15);
            this.buttonResetZoom.Name = "buttonResetZoom";
            this.buttonResetZoom.Size = new System.Drawing.Size(78, 20);
            this.buttonResetZoom.TabIndex = 34;
            this.buttonResetZoom.Text = "Zoom to fit";
            this.buttonResetZoom.UseVisualStyleBackColor = true;
            this.buttonResetZoom.Click += new System.EventHandler(this.buttonResetZoom_Click);
            // 
            // textCursorPos2
            // 
            this.textCursorPos2.Location = new System.Drawing.Point(365, 64);
            this.textCursorPos2.Name = "textCursorPos2";
            this.textCursorPos2.Size = new System.Drawing.Size(57, 20);
            this.textCursorPos2.TabIndex = 33;
            this.textCursorPos2.TextChanged += new System.EventHandler(this.textCursorPos2_TextChanged);
            // 
            // textCursorPos1
            // 
            this.textCursorPos1.Location = new System.Drawing.Point(365, 46);
            this.textCursorPos1.Name = "textCursorPos1";
            this.textCursorPos1.Size = new System.Drawing.Size(57, 20);
            this.textCursorPos1.TabIndex = 32;
            this.textCursorPos1.TextChanged += new System.EventHandler(this.textCursorPos1_TextChanged);
            // 
            // buttonCursor2
            // 
            this.buttonCursor2.Location = new System.Drawing.Point(308, 65);
            this.buttonCursor2.Name = "buttonCursor2";
            this.buttonCursor2.Size = new System.Drawing.Size(56, 20);
            this.buttonCursor2.TabIndex = 31;
            this.buttonCursor2.Text = "Cursor 2";
            this.buttonCursor2.UseVisualStyleBackColor = true;
            this.buttonCursor2.Click += new System.EventHandler(this.buttonCursor2_Click);
            // 
            // buttonCursor1
            // 
            this.buttonCursor1.Location = new System.Drawing.Point(308, 45);
            this.buttonCursor1.Name = "buttonCursor1";
            this.buttonCursor1.Size = new System.Drawing.Size(56, 20);
            this.buttonCursor1.TabIndex = 30;
            this.buttonCursor1.Text = "Cursor 1";
            this.buttonCursor1.UseVisualStyleBackColor = true;
            this.buttonCursor1.Click += new System.EventHandler(this.buttonCursor1_Click);
            // 
            // labelCursor
            // 
            this.labelCursor.AutoSize = true;
            this.labelCursor.Location = new System.Drawing.Point(306, 89);
            this.labelCursor.Name = "labelCursor";
            this.labelCursor.Size = new System.Drawing.Size(23, 26);
            this.labelCursor.TabIndex = 29;
            this.labelCursor.Text = "Diff\r\nDiff";
            // 
            // textReject
            // 
            this.textReject.Location = new System.Drawing.Point(214, 16);
            this.textReject.Margin = new System.Windows.Forms.Padding(2);
            this.textReject.Name = "textReject";
            this.textReject.Size = new System.Drawing.Size(41, 20);
            this.textReject.TabIndex = 27;
            this.textReject.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(161, 19);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(50, 13);
            this.label15.TabIndex = 28;
            this.label15.Text = "Rejected";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonSendCommand);
            this.groupBox2.Controls.Add(this.textCmd1);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.textCmd4);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.textCmd3);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.textCmd2);
            this.groupBox2.Location = new System.Drawing.Point(9, 35);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(181, 117);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Command";
            // 
            // buttonSendCommand
            // 
            this.buttonSendCommand.Location = new System.Drawing.Point(117, 15);
            this.buttonSendCommand.Margin = new System.Windows.Forms.Padding(2);
            this.buttonSendCommand.Name = "buttonSendCommand";
            this.buttonSendCommand.Size = new System.Drawing.Size(56, 19);
            this.buttonSendCommand.TabIndex = 27;
            this.buttonSendCommand.Text = "Send";
            this.buttonSendCommand.UseVisualStyleBackColor = true;
            this.buttonSendCommand.Click += new System.EventHandler(this.buttonSendCommand_Click);
            // 
            // textCmd1
            // 
            this.textCmd1.Location = new System.Drawing.Point(41, 17);
            this.textCmd1.Margin = new System.Windows.Forms.Padding(2);
            this.textCmd1.Name = "textCmd1";
            this.textCmd1.Size = new System.Drawing.Size(72, 20);
            this.textCmd1.TabIndex = 18;
            this.textCmd1.Text = "0";
            this.textCmd1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 20);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(28, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "CH1";
            // 
            // textCmd4
            // 
            this.textCmd4.Location = new System.Drawing.Point(41, 85);
            this.textCmd4.Margin = new System.Windows.Forms.Padding(2);
            this.textCmd4.Name = "textCmd4";
            this.textCmd4.Size = new System.Drawing.Size(72, 20);
            this.textCmd4.TabIndex = 20;
            this.textCmd4.Text = "0";
            this.textCmd4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(10, 88);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(28, 13);
            this.label12.TabIndex = 21;
            this.label12.Text = "CH4";
            // 
            // textCmd3
            // 
            this.textCmd3.Location = new System.Drawing.Point(41, 63);
            this.textCmd3.Margin = new System.Windows.Forms.Padding(2);
            this.textCmd3.Name = "textCmd3";
            this.textCmd3.Size = new System.Drawing.Size(72, 20);
            this.textCmd3.TabIndex = 22;
            this.textCmd3.Text = "0";
            this.textCmd3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(10, 42);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(28, 13);
            this.label13.TabIndex = 25;
            this.label13.Text = "CH2";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(10, 65);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(28, 13);
            this.label14.TabIndex = 23;
            this.label14.Text = "CH3";
            // 
            // textCmd2
            // 
            this.textCmd2.Location = new System.Drawing.Point(41, 40);
            this.textCmd2.Margin = new System.Windows.Forms.Padding(2);
            this.textCmd2.Name = "textCmd2";
            this.textCmd2.Size = new System.Drawing.Size(72, 20);
            this.textCmd2.TabIndex = 24;
            this.textCmd2.Text = "0";
            this.textCmd2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // comboDuration
            // 
            this.comboDuration.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboDuration.FormattingEnabled = true;
            this.comboDuration.Items.AddRange(new object[] {
            "100",
            "200",
            "500",
            "1000",
            "2000",
            "5000",
            "10000"});
            this.comboDuration.Location = new System.Drawing.Point(69, 15);
            this.comboDuration.Margin = new System.Windows.Forms.Padding(2);
            this.comboDuration.Name = "comboDuration";
            this.comboDuration.Size = new System.Drawing.Size(56, 21);
            this.comboDuration.TabIndex = 28;
            // 
            // timerDraw
            // 
            this.timerDraw.Enabled = true;
            this.timerDraw.Interval = 16;
            this.timerDraw.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.comboTrig);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.radioTrig);
            this.groupBox3.Controls.Add(this.radioScroll);
            this.groupBox3.Controls.Add(this.comboDuration);
            this.groupBox3.Location = new System.Drawing.Point(195, 37);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(171, 114);
            this.groupBox3.TabIndex = 35;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Acq";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 82);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(58, 13);
            this.label17.TabIndex = 33;
            this.label17.Text = "Trigger CH";
            // 
            // comboTrig
            // 
            this.comboTrig.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboTrig.FormattingEnabled = true;
            this.comboTrig.Items.AddRange(new object[] {
            "CH1",
            "CH2",
            "CH3",
            "CH4",
            "CH5",
            "CH6",
            "CH7",
            "CH8"});
            this.comboTrig.Location = new System.Drawing.Point(69, 75);
            this.comboTrig.Margin = new System.Windows.Forms.Padding(2);
            this.comboTrig.Name = "comboTrig";
            this.comboTrig.Size = new System.Drawing.Size(56, 21);
            this.comboTrig.TabIndex = 32;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 22);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(56, 13);
            this.label16.TabIndex = 31;
            this.label16.Text = "Buffer size";
            // 
            // radioTrig
            // 
            this.radioTrig.AutoSize = true;
            this.radioTrig.Location = new System.Drawing.Point(67, 41);
            this.radioTrig.Name = "radioTrig";
            this.radioTrig.Size = new System.Drawing.Size(58, 17);
            this.radioTrig.TabIndex = 30;
            this.radioTrig.Text = "Trigger";
            this.radioTrig.UseVisualStyleBackColor = true;
            // 
            // radioScroll
            // 
            this.radioScroll.AutoSize = true;
            this.radioScroll.Checked = true;
            this.radioScroll.Location = new System.Drawing.Point(10, 41);
            this.radioScroll.Name = "radioScroll";
            this.radioScroll.Size = new System.Drawing.Size(51, 17);
            this.radioScroll.TabIndex = 29;
            this.radioScroll.TabStop = true;
            this.radioScroll.Text = "Scroll";
            this.radioScroll.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 448);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.refreshBtn);
            this.Controls.Add(this.connectBtn);
            this.Controls.Add(this.serialPortList);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "MainForm";
            this.Text = "UART Monitor";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button refreshBtn;
        private System.Windows.Forms.Button connectBtn;
        private System.Windows.Forms.ComboBox serialPortList;
        private System.Windows.Forms.TextBox textRate;
        private System.IO.Ports.SerialPort serialPort;
        private Arction.WinForms.Charting.LightningChartUltimate chart1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textCH1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textCH4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textCH3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textCH2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textCH6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textCH7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textCH8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textCH5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonSendCommand;
        private System.Windows.Forms.TextBox textCmd1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textCmd4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textCmd3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textCmd2;
        private System.Windows.Forms.TextBox textReject;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textCursorPos2;
        private System.Windows.Forms.TextBox textCursorPos1;
        private System.Windows.Forms.Button buttonCursor2;
        private System.Windows.Forms.Button buttonCursor1;
        private System.Windows.Forms.Label labelCursor;
        private System.Windows.Forms.ComboBox comboDuration;
        private System.Windows.Forms.Button buttonResetZoom;
        private System.Windows.Forms.Timer timerDraw;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox comboTrig;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.RadioButton radioTrig;
        private System.Windows.Forms.RadioButton radioScroll;
    }
}


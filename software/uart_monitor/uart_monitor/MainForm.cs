﻿using Arction.WinForms.Charting;
using Arction.WinForms.Charting.SeriesXY;
using Arction.WinForms.Charting.Views.ViewXY;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using org.mariuszgromada.math.mxparser;

namespace uart_monitor
{
    public partial class MainForm : Form
    {
        public bool connected = false;

        public MainForm() {
            InitializeComponent();

            chart1.ViewXY.LineSeriesCursors[0].PositionChanged += UpdateCursorInfo;
            chart1.ViewXY.LineSeriesCursors[1].PositionChanged += UpdateCursorInfo;
            UpdateCursorInfo();
        }

        private void Log(string msg) {
        }
        private void Log(string msg, Color color) {
        }

        private void MainForm_Load(object sender, EventArgs e) {
            comboDuration.SelectedIndex = 3;
            comboTrig.SelectedIndex = 0;
            refreshBtn_Click(sender, e);
        }

        private void connectBtn_Click(object sender, EventArgs e) {
            lock (chart_lock) {
                var xaxe = chart1.ViewXY.XAxes[0];
                if (!connected) {

                    // Start
                    chart1.BeginUpdate();
                    if (true) {
                        foreach (var series in chart1.ViewXY.PointLineSeries)
                            series.Clear();
                        xaxe.ScrollPosition = 0;
                        chart1.ViewXY.LineSeriesCursors[0].ValueAtXAxis = xaxe.ScrollPosition;
                        lock (buf_lock) {
                            buftail = 0;
                        }
                        counter = 0;
                        reject_count = 0;
                        last_sample_time = -1;
                        counter_start.Restart();
                    }
                    chart1.ViewXY.LineSeriesCursors[1].ValueAtXAxis = xaxe.ScrollPosition;
                    chart1.Options.MouseInteraction = false;
                    chart1.ViewXY.DropOldSeriesData = true;
                    xaxe.SetRange(xaxe.ScrollPosition - Double.Parse(comboDuration.Text), xaxe.ScrollPosition);

                    if (radioScroll.Checked) {
                        xaxe.ScrollMode = XAxisScrollMode.Scrolling;
                    } else {
                        xaxe.ScrollMode = XAxisScrollMode.Triggering;
                        xaxe.Triggering.StaticMajorXGridOptions.Visible = true;
                        xaxe.Triggering.StaticMajorXGridOptions.DivCount = 12;
                        xaxe.Triggering.TriggerSeriesType = TriggeringSeriesType.PointLineSeries;
                        xaxe.Triggering.TriggerLineSeriesIndex = comboTrig.SelectedIndex;
                        xaxe.Triggering.TriggeringActive = true;
                        xaxe.Triggering.TriggeringXPosition = 50;
                        xaxe.Triggering.Edge = TriggeringEdge.Raising;
                    }
                    chart1.EndUpdate();


                    try {
                        serialPort.PortName = serialPortList.Text;
                        serialPort.Open();
                        serialPort.DiscardInBuffer();
                        connected = true;
                        connectBtn.Text = "Disconnect";
                        Log("Connected to " + serialPort.PortName);
                    } catch (ArgumentException) {
                        Log("Bad port name");
                        serialPort.Close();
                    } catch (UnauthorizedAccessException) {
                        Log("Can not open " + serialPort.PortName);
                        serialPort.Close();
                    } catch (IOException) {
                        Log("Can not open " + serialPort.PortName);
                        serialPort.Close();
                    }
                } else {
                    serialPort.Close();
                    Log("Port closed");
                    connected = false;
                    connectBtn.Text = "Connect";

                    // Stop
                    chart1.BeginUpdate();
                    chart1.ViewXY.DropOldSeriesData = false;
                    xaxe.ScrollMode = XAxisScrollMode.None;
                    chart1.ViewXY.LineSeriesCursors[0].ValueAtXAxis = xaxe.ScrollPosition;
                    chart1.Options.MouseInteraction = true;
                    chart1.EndUpdate();
                }
            }
        }

        private void refreshBtn_Click(object sender, EventArgs e) {
            var old_port = serialPortList.Text;
            var ports = SerialPort.GetPortNames();
            serialPortList.Items.Clear();
            foreach (var port in ports) {
                serialPortList.Items.Add(port);
            }
            var index = serialPortList.Items.IndexOf(old_port);
            if (index == -1 && serialPortList.Items.Count > 0) {
                serialPortList.Text = (string)serialPortList.Items[0];
            }
        }

        private void serialPortList_KeyPress(object sender, KeyPressEventArgs e) {
            if (e.KeyChar == '\r') {
                connectBtn_Click(sender, e);
                e.Handled = true;
            }
        }

        public const int CHANNEL_NUM = 8;
        public const int GRAPH_BUFFER_COUNT = 4096;

        private double[] time_buf = new double[GRAPH_BUFFER_COUNT];
        private double[,] value_buf = new double[CHANNEL_NUM, GRAPH_BUFFER_COUNT];
        private readonly object chart_lock = new object();
        private readonly object buf_lock = new object();
        private int buftail = 0;

        private int counter = 0;
        private int reject_count = 0;
        private int last_sample_time = -1;
        private Stopwatch counter_start = new Stopwatch();
        
        private int stage = 0;
        private byte[] payload = new byte[4 * CHANNEL_NUM + 4];
        private UInt16 payload_time = 0;
        private float[] payload_data = new float[CHANNEL_NUM];

        private void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e) {
            try {
                var len = serialPort.BytesToRead;
                var buf = new byte[len];
                int recv_len = serialPort.Read(buf, 0, len);

                for (int i = 0; i < recv_len; i++) {
                    switch (stage) {
                        case 0:
                            if (buf[i] == 'D')
                                stage = 1;
                            break;
                        case 1:
                            if (buf[i] == 'T')
                                stage = 2;
                            else
                                stage = 0;
                            break;
                        default:
                            var tofill = Math.Min(6 + (4 * CHANNEL_NUM) - stage, recv_len - i);
                            Array.Copy(buf, i, payload, stage - 2, tofill);
                            stage += tofill;
                            i += tofill - 1;
                            if (stage == 6 + (4 * CHANNEL_NUM)) { // checksum
                                stage = 0;

                                int checksum = 0;
                                for (int j = 0; j < payload.Length - 2; j += 2) {
                                    checksum += System.BitConverter.ToUInt16(payload, j);
                                }
                                checksum &= 0xFFFF;
                                checksum -= System.BitConverter.ToUInt16(payload, payload.Length - 2);
                                if (checksum == 0) {
                                    payload_time = System.BitConverter.ToUInt16(payload, 0);
                                    for (int j = 0; j < payload_data.Length; j++) {
                                        payload_data[j] = System.BitConverter.ToSingle(payload, 2 + 4 * j);
                                    }
                                    // Process sample
                                    if (last_sample_time == -1) {
                                        last_sample_time = payload_time;
                                    } else {
                                        var diff = (UInt16)(payload_time - last_sample_time);
                                        last_sample_time = payload_time;
                                        counter += diff;
                                        if (diff > 1)
                                            reject_count += diff - 1;
                                    }
                                    if (counter % 4 == 0) {
                                        lock (buf_lock) {
                                            time_buf[buftail] = counter;
                                            for (int ch = 0; ch < CHANNEL_NUM; ch++)
                                                value_buf[ch, buftail] = payload_data[ch];
                                            buftail++;
                                        }
                                        if (buftail == time_buf.Length) {
                                            flushChartBuffer();
                                        }
                                    }
                                } else
                                    reject_count++;
                            }
                            break;
                    }
                }
            } catch (InvalidOperationException) { }
        }

        private void timer1_Tick(object sender, EventArgs e) {
            if (connected) {
                flushChartBuffer();
                var rate = counter * 1000.0 / counter_start.ElapsedMilliseconds;
                textRate.Text = $"{rate:0.0}";
                textReject.Text = reject_count.ToString();
                textCH1.Text = payload_data[0].ToString("0.000");
                textCH2.Text = payload_data[1].ToString("0.000");
                textCH3.Text = payload_data[2].ToString("0.000");
                textCH4.Text = payload_data[3].ToString("0.000");
                textCH5.Text = payload_data[4].ToString("0.000");
                textCH6.Text = payload_data[5].ToString("0.000");
                textCH7.Text = payload_data[6].ToString("0.000");
                textCH8.Text = payload_data[7].ToString("0.000");
                UpdateCursorInfo();
            }
        }

        private void flushChartBuffer() {
            lock (chart_lock) {
                if (!connected) return;
                if (buftail == 0)
                    return;

                chart1.BeginUpdate();
                lock (buf_lock) {
                    var time = new double[buftail];
                    var value = new double[buftail];

                    Array.Copy(time_buf, time, buftail);

                    for (int ch = 0; ch < CHANNEL_NUM; ch++) {
                        Buffer.BlockCopy(value_buf, GRAPH_BUFFER_COUNT * ch * sizeof(double), value, 0, buftail * sizeof(double));
                        chart1.ViewXY.PointLineSeries[ch].AddPoints(time, value, false);
                    }

                    chart1.ViewXY.XAxes[0].ScrollPosition = time[time.Length - 1];
                    buftail = 0;
                }
                chart1.EndUpdate();
            }
        }

        public const int TX_CHANNEL_NUM = 4;
        private void buttonSendCommand_Click(object sender, EventArgs e) {
            var tx_payload = new UInt16[TX_CHANNEL_NUM];
            try {
                tx_payload[0] = (UInt16)ParseInputI16(textCmd1.Text);
                tx_payload[1] = (UInt16)ParseInputI16(textCmd2.Text);
                tx_payload[2] = (UInt16)ParseInputI16(textCmd3.Text);
                tx_payload[3] = (UInt16)ParseInputI16(textCmd4.Text);
            } catch (FormatException) {
                MessageBox.Show("Invalid data", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            } catch (OverflowException) {
                MessageBox.Show("Invalid data", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var tx_buffer = new byte[3 + 2 * TX_CHANNEL_NUM];
            Encoding.ASCII.GetBytes("CMD").CopyTo(tx_buffer, 0);
            for (int i = 0; i < TX_CHANNEL_NUM; i++) {
                tx_buffer[3 + 2 * i] = (byte)(tx_payload[i] & 0xFF);
                tx_buffer[4 + 2 * i] = (byte)(tx_payload[i] >> 8);
            }
            serialPort.Write(tx_buffer, 0, tx_buffer.Length);
        }

        private void buttonCursor1_Click(object sender, EventArgs e) {
            chart1.BeginUpdate();
            chart1.ViewXY.LineSeriesCursors[0].ValueAtXAxis = (chart1.ViewXY.XAxes[0].Minimum + chart1.ViewXY.XAxes[0].Maximum) / 2;
            chart1.EndUpdate();
        }

        private void buttonCursor2_Click(object sender, EventArgs e) {
            chart1.BeginUpdate();
            chart1.ViewXY.LineSeriesCursors[1].ValueAtXAxis = (chart1.ViewXY.XAxes[0].Minimum + chart1.ViewXY.XAxes[0].Maximum) / 2;
            chart1.EndUpdate();
        }
        private bool cursor_updating = false;
        private void UpdateCursorInfo(object sender, PositionChangedEventArgs e) {
            UpdateCursorInfo();
        }
        private void UpdateCursorInfo() {
            var cursor1 = chart1.ViewXY.LineSeriesCursors[0].ValueAtXAxis;
            var cursor2 = chart1.ViewXY.LineSeriesCursors[1].ValueAtXAxis;
            var diff = Math.Abs(cursor1 - cursor2);
            if (!cursor_updating) {
                textCursorPos1.Text = $"{cursor1:0.00}";
                textCursorPos2.Text = $"{cursor2:0.00}";
            }
            labelCursor.Text =
                $"Diff: {diff:0.00} ms\r\n" +
                $"Diff: {1000 / diff:0.00} Hz";
            if (!connected) {
                textCH1.Text = SolveValue(chart1.ViewXY.PointLineSeries[0], cursor1).ToString("0.000");
                textCH2.Text = SolveValue(chart1.ViewXY.PointLineSeries[1], cursor1).ToString("0.000");
                textCH3.Text = SolveValue(chart1.ViewXY.PointLineSeries[2], cursor1).ToString("0.000");
                textCH4.Text = SolveValue(chart1.ViewXY.PointLineSeries[3], cursor1).ToString("0.000");
                textCH5.Text = SolveValue(chart1.ViewXY.PointLineSeries[4], cursor1).ToString("0.000");
                textCH6.Text = SolveValue(chart1.ViewXY.PointLineSeries[5], cursor1).ToString("0.000");
                textCH7.Text = SolveValue(chart1.ViewXY.PointLineSeries[6], cursor1).ToString("0.000");
                textCH8.Text = SolveValue(chart1.ViewXY.PointLineSeries[7], cursor1).ToString("0.000");
            }
        }

        private void textCursorPos1_TextChanged(object sender, EventArgs e) {
            cursor_updating = true;
            try {
                chart1.BeginUpdate();
                chart1.ViewXY.LineSeriesCursors[0].ValueAtXAxis = double.Parse(textCursorPos1.Text);
                chart1.EndUpdate();
            } catch (FormatException) { } catch (OverflowException) { }
            cursor_updating = false;
        }

        private void textCursorPos2_TextChanged(object sender, EventArgs e) {
            cursor_updating = true;
            try {
                chart1.BeginUpdate();
                chart1.ViewXY.LineSeriesCursors[1].ValueAtXAxis = double.Parse(textCursorPos2.Text);
                chart1.EndUpdate();
            } catch (FormatException) { } catch (OverflowException) { }
            cursor_updating = false;
        }

        private void buttonResetZoom_Click(object sender, EventArgs e) {
            chart1.BeginUpdate();
            chart1.ViewXY.ZoomToFit();
            chart1.EndUpdate();
        }

        private double SolveValue(PointLineSeries series, double xValue) {
            LineSeriesValueSolveResult result = series.SolveYValueAtXValue(xValue);
            if (result.SolveStatus == LineSeriesSolveStatus.OK) {
                //PointLineSeries may have two or more points at same X value. If so, center it between min and max 
                return (result.YMax + result.YMin) / 2.0;
            } else {
                return Double.NaN;
            }
        }


        private double ParseInput(string inp) {
            var exp = new Expression(inp).calculate();
            if (Double.IsNaN(exp) || Double.IsInfinity(exp))
                throw new FormatException();
            return exp;
        }
        private Int16 ParseInputI16(string inp) {
            return Convert.ToInt16(ParseInput(inp));
        }
        private double ParseInput(string inp, double multiplier, out Int16 outp) {
            var exp = ParseInput(inp);
            outp = Convert.ToInt16(exp * multiplier);
            return outp / multiplier;
        }
        private double ParseInput(string inp, double multiplier, out UInt16 outp) {
            var exp = ParseInput(inp);
            outp = Convert.ToUInt16(exp * multiplier);
            return outp / multiplier;
        }
        private double ParseInput(string inp, double multiplier, out Int32 outp) {
            var exp = ParseInput(inp);
            outp = Convert.ToInt32(exp * multiplier);
            return outp / multiplier;
        }
    }
}


/*
 * Include Files
 *
 */
#if defined(MATLAB_MEX_FILE)
#include "tmwtypes.h"
#include "simstruc_types.h"
#else
#include "rtwtypes.h"
#endif



/* %%%-SFUNWIZ_wrapper_includes_Changes_BEGIN --- EDIT HERE TO _END */
#include <stdint.h>
#define NUM_VARS 4

#ifdef F28335
#include "DSP2833x_Device.h"
#define CODE_GEN 1
#endif
/* %%%-SFUNWIZ_wrapper_includes_Changes_END --- EDIT HERE TO _BEGIN */
#define y_width 1

/*
 * Create external references here.  
 *
 */
/* %%%-SFUNWIZ_wrapper_externs_Changes_BEGIN --- EDIT HERE TO _END */
 
/* %%%-SFUNWIZ_wrapper_externs_Changes_END --- EDIT HERE TO _BEGIN */

/*
 * Start function
 *
 */
void sci_rx_Start_wrapper(void)
{
/* %%%-SFUNWIZ_wrapper_Start_Changes_BEGIN --- EDIT HERE TO _END */
// SCIRX Start
#ifdef CODE_GEN
EALLOW;
GpioCtrlRegs.GPAPUD.bit.GPIO28 = 0;  // Enable pull-up for GPIO28 (SCIRXDA)
GpioCtrlRegs.GPAQSEL2.bit.GPIO28 = 3;  // Asynch input GPIO28 (SCIRXDA)
GpioCtrlRegs.GPAMUX2.bit.GPIO28 = 1;   // Configure GPIO28 to SCIRXDA 

SciaRegs.SCICTL1.bit.RXENA = 1;
SciaRegs.SCIFFRX.bit.RXFIFORESET = 1;
SciaRegs.SCICTL1.bit.SWRESET = 0;
SciaRegs.SCICTL1.bit.SWRESET = 1;
EDIS;
#endif
/* %%%-SFUNWIZ_wrapper_Start_Changes_END --- EDIT HERE TO _BEGIN */
}
/*
 * Output function
 *
 */
void sci_rx_Outputs_wrapper(int16_T *y0)
{
/* %%%-SFUNWIZ_wrapper_Outputs_Changes_BEGIN --- EDIT HERE TO _END */
/* This sample sets the output equal to the input
      y0[0] = u0[0]; 
 For complex signals use: y0[0].re = u0[0].re; 
      y0[0].im = u0[0].im;
      y1[0].re = u1[0].re;
      y1[0].im = u1[0].im;
 */
static uint16_t state = 0;
int i;
if(state == 0){
    // Reset
    for(i=0;i<NUM_VARS;i++)
        y0[i] = 0;
    state = 1;
}

#ifdef CODE_GEN
static uint16_t payload[NUM_VARS * 2];
static uint16_t payload_length = 0;
uint16_t buf;
while(SciaRegs.SCIFFRX.bit.RXFFST){
    buf = SciaRegs.SCIRXBUF.all & 0xFF;
    switch(state){
        case 1: // Get C
            if(buf == 'C')
                state = 2;
            break;
        case 2:
            if(buf == 'M')
                state = 3;
            else
                state = 1;
            break;
        case 3:
            if(buf == 'D'){
                state = 4;
                payload_length = 0;
            }else
                state = 1;
            break;
        case 4: // Start receiving
            payload[payload_length++] = buf;
            if(payload_length == NUM_VARS * 2){ // Checksum
                //uint16_t checksum = 0;
                for(i=0;i<NUM_VARS;i++)
                    y0[i] = (int16_t)(payload[i*2] + (payload[i*2+1] << 8));
                state = 1;
            }
            break;
    }
}
#endif
/* %%%-SFUNWIZ_wrapper_Outputs_Changes_END --- EDIT HERE TO _BEGIN */
}




/*
 * Include Files
 *
 */
#if defined(MATLAB_MEX_FILE)
#include "tmwtypes.h"
#include "simstruc_types.h"
#else
#include "rtwtypes.h"
#endif



/* %%%-SFUNWIZ_wrapper_includes_Changes_BEGIN --- EDIT HERE TO _END */
#include <stdint.h>

#ifdef F28335
#include "DSP2833x_Device.h"
#define CODE_GEN 1
#endif
/* %%%-SFUNWIZ_wrapper_includes_Changes_END --- EDIT HERE TO _BEGIN */
#define u_width 19

/*
 * Create external references here.  
 *
 */
/* %%%-SFUNWIZ_wrapper_externs_Changes_BEGIN --- EDIT HERE TO _END */
 
/* %%%-SFUNWIZ_wrapper_externs_Changes_END --- EDIT HERE TO _BEGIN */

/*
 * Start function
 *
 */
void sci_tx_Start_wrapper(void)
{
/* %%%-SFUNWIZ_wrapper_Start_Changes_BEGIN --- EDIT HERE TO _END */
// SCITX Start
#ifdef CODE_GEN
/* GPIO27
EALLOW;
GpioCtrlRegs.GPAMUX2.all &= 0xFF3FFFFF;
GpioCtrlRegs.GPADIR.all |= 0x8000000;
EDIS;
*/
#endif
/* %%%-SFUNWIZ_wrapper_Start_Changes_END --- EDIT HERE TO _BEGIN */
}
/*
 * Output function
 *
 */
void sci_tx_Outputs_wrapper(const uint16_T *x0)
{
/* %%%-SFUNWIZ_wrapper_Outputs_Changes_BEGIN --- EDIT HERE TO _END */
static uint16_t divider = 4;
static uint16_t buffer[19*2], buf_head = 0;
int i;
#ifdef CODE_GEN
//GpioDataRegs.GPASET.bit.GPIO27 = 1;
while (buf_head != 19*2 && SciaRegs.SCIFFTX.bit.TXFFST != 16) {
    SciaRegs.SCITXBUF = buffer[buf_head++];
}
if(--divider == 0){
    divider = 4;
    // Load data to buffer
    for (i=0;i<19;i++){
        buffer[i*2] = x0[i] & 0xFF;
        buffer[i*2+1] = x0[i] >> 8;
    }
    buf_head = 0;
}
//GpioDataRegs.GPACLEAR.bit.GPIO27 = 1;
#endif
/* %%%-SFUNWIZ_wrapper_Outputs_Changes_END --- EDIT HERE TO _BEGIN */
}


